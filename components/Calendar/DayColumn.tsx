import React from 'react';
import { format } from 'date-fns';
import { FaBed, FaChair } from 'react-icons/fa';
import { cs } from 'date-fns/locale';
import TimeSlotButton from '@/components/Calendar/TimeSlotButton';

interface DayColumnProps {
  day: Date;
  times: string[];
  chamberType: string[];
  handleTimeSlotClick: (day: Date, time: string, chamberType: string) => void;
  possibleStartsLay: string[];
  possibleStartsSit: string[];
  allPeriodAppointments: {
    id: number;
    date: Date;
    userId: number;
    chamberType: string;
  }[];
  disabled: boolean;
}

const DayColumn: React.FC<DayColumnProps> = ({
  day,
  times,
  chamberType,
  handleTimeSlotClick,
  possibleStartsLay,
  possibleStartsSit,
  allPeriodAppointments,
  disabled,
}) => {
  const capitalizeFirstLetter = (stringToCapitalize: string) =>
    stringToCapitalize.charAt(0).toUpperCase() + stringToCapitalize.slice(1);
  return (
    <div className="flex flex-col min-w-32">
      <div className="p-2 h-8 font-bold justify-center flex">
        {capitalizeFirstLetter(format(day, 'EEEE', { locale: cs }))}
      </div>
      <div className="p-2 h-8 justify-center flex">
        {format(day, 'd. MMMM', { locale: cs })}
      </div>
      <div className="flex h-12">
        <div className="flex-1 flex items-center justify-center">
          <FaBed style={{ width: '50%', height: '50%' }} />
        </div>
        <div className="flex-1 flex items-center justify-center">
          <FaChair style={{ width: '50%', height: '50%' }} />
        </div>
      </div>
      <div className="flex">
        {chamberType.map((type, typeIndex) => (
          <div key={typeIndex} className="flex flex-col w-1/2 items-center">
            {type === 'sit' ? (
              <>
                <div className="h-24"></div>
                {/* Spacer for sit chamber */}
                {times
                  .filter((time) => possibleStartsSit.includes(time))
                  .map((time, timeIndex) => (
                    <TimeSlotButton
                      key={timeIndex}
                      day={day}
                      time={time}
                      chamberType={type}
                      handleTimeSlotClick={handleTimeSlotClick}
                      allPeriodAppointments={allPeriodAppointments}
                      disabled={disabled}
                    />
                  ))}
              </>
            ) : (
              times
                .filter((time) => possibleStartsLay.includes(time))
                .map((time, timeIndex) => (
                  <TimeSlotButton
                    key={timeIndex}
                    day={day}
                    time={time}
                    chamberType={type}
                    handleTimeSlotClick={handleTimeSlotClick}
                    allPeriodAppointments={allPeriodAppointments}
                    disabled={disabled}
                  />
                ))
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default DayColumn;
