'use client';
import React, { Suspense, useCallback, useEffect, useState } from 'react';
import { format } from 'date-fns';
import { cs } from 'date-fns/locale';
import TimeMarkings from '@/components/Calendar/TimeMarkings';
import DayColumn from '@/components/Calendar/DayColumn';
import { useSession } from 'next-auth/react';
import { showToast } from '@/lib/toastUtils';
import { Divider } from '@chakra-ui/react';
import LoadingSpinner from '@/components/LoadingSpinner';
import { getDatePart } from '@/lib/dates';

type CalendarBodyProps = {
  currentPeriod: Date[];
  disabled: boolean;
};

function generateTimeMarkings(startTime: string, endTime: string): string[] {
  let [startHour, startMinute] = startTime.split(':').map(Number);
  let [endHour, endMinute] = endTime.split(':').map(Number);
  const times: string[] = [];

  const formatTime = (hour: number, minute: number): string =>
    `${hour}:${minute.toString().padStart(2, '0')}`;

  for (
    let currentMinute = startHour * 60 + startMinute;
    currentMinute <= endHour * 60 + endMinute;
    currentMinute += 15
  ) {
    const hour: number = Math.floor(currentMinute / 60);
    const minute: number = currentMinute % 60;
    times.push(formatTime(hour, minute));
  }

  return times;
}
const startTime = '09:00';
const endTime = '17:00';
const times = generateTimeMarkings(startTime, endTime);
const possibleStartsLay = ['9:00', '10:15', '11:30', '12:45', '14:00', '15:15'];
const possibleStartsSit = ['9:30', '10:45', '12:00', '13:15', '14:30', '15:45'];
const chamberType = ['lay', 'sit'];

const CalendarBody: React.FC<CalendarBodyProps> = ({
  currentPeriod,
  disabled,
}) => {
  const { data: session } = useSession();
  const [appointments, setAppointments] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const fetchAppointments = useCallback(
    async (shouldShowLoading: boolean = true) => {
      if (shouldShowLoading) {
        setIsLoading(true);
      }

      const start: Date = new Date(currentPeriod[0]);
      const startDateString = getDatePart(start);

      const end = new Date(currentPeriod[currentPeriod.length - 1]);
      const endDateString = getDatePart(end);

      const url = `/api/appointments?startDate=${startDateString}&endDate=${endDateString}`;
      try {
        const res = await fetch(url, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        });
        const responseData = await res.json();
        if (responseData && responseData.appointments) {
          setAppointments(responseData.appointments);
        }
      } catch (error) {
        console.error('Failed to fetch appointments', error);
      }
      if (shouldShowLoading) {
        setIsLoading(false);
      }
    },
    [currentPeriod],
  );

  useEffect(() => {
    fetchAppointments();
  }, [fetchAppointments]);

  const handleTimeSlotClick = async (
    day: Date,
    time: string,
    chamberType: string,
  ) => {
    const [hours, minutes] = time.split(':').map(Number);
    const localDate = new Date(day);
    localDate.setHours(hours, minutes, 0, 0);

    setIsLoading(true);
    const res = await fetch(`/api/user/appointment`, {
      method: 'POST',
      body: JSON.stringify({
        date: localDate,
        email: session?.user?.email,
        chamberType: chamberType,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const responseData = await res.json();

    if (res.ok) {
      const appointmentDate = new Date(responseData.appointment.date);
      const dateOptions: Intl.DateTimeFormatOptions = {
        weekday: 'long',
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
      };
      const formattedDate = new Intl.DateTimeFormat(
        'cs-CZ',
        dateOptions,
      ).format(appointmentDate);

      const timeOptions: Intl.DateTimeFormatOptions = {
        hour: 'numeric',
        minute: 'numeric',
        hour12: false,
      };
      const formattedTime = new Intl.DateTimeFormat(
        'cs-CZ',
        timeOptions,
      ).format(appointmentDate);
      const finalDateTime = `${formattedDate} v ${formattedTime}`;
      setIsLoading(false);

      showToast('Zarezervováno', 'success', `Datum: ${finalDateTime}`);
      await fetchAppointments(false);
    } else {
      showToast('Rezervace se nezdařila', 'error', responseData.message);
    }
  };

  if (isLoading) {
    return <LoadingSpinner />;
  }
  return (
    <div className="flex justify-center">
      <TimeMarkings times={times} />
      <Divider
        orientation={'vertical'}
        variant={'dashed'}
        className={'ml-2 mr-2'}
        borderWidth={'2px'}
        borderColor={'blue.200'}
      />

      {currentPeriod.map((day, dayIndex) => (
        <div className={'flex'} key={dayIndex}>
          <DayColumn
            day={day}
            times={times}
            chamberType={chamberType}
            handleTimeSlotClick={handleTimeSlotClick}
            possibleStartsLay={possibleStartsLay}
            possibleStartsSit={possibleStartsSit}
            allPeriodAppointments={appointments}
            disabled={disabled}
          />
          <Divider
            orientation={'vertical'}
            variant={'dashed'}
            className={'ml-2 mr-2'}
            borderWidth={'2px'}
            borderColor={'blue.200'}
          />
        </div>
      ))}
    </div>
  );
};

export default CalendarBody;
