'use client';
import { eachDayOfInterval, addDays, subDays, startOfWeek } from 'date-fns';
import React, { Suspense } from 'react';
import CalendarHeader from '@/components/Calendar/CalendarHeader';
import CalendarBody from '@/components/Calendar/CalendarBody';
import LoadingSpinner from '@/components/LoadingSpinner';

type CalendarProps = {
  daysAmount: number;
  disabled: boolean;
};

const Calendar: React.FC<CalendarProps> = ({ daysAmount, disabled }) => {
  const getCalendarDays = (date: Date) => {
    let start = date;
    let end;

    if (daysAmount === 7) {
      start = startOfWeek(date, { weekStartsOn: 1 });
      end = addDays(start, 6);
    } else {
      end = addDays(date, daysAmount - 1);
    }

    return eachDayOfInterval({ start, end });
  };

  const [currentCalendarPeriod, setCurrentPeriod] = React.useState<Date[]>(() =>
    getCalendarDays(new Date()),
  );

  const handlePeriodChange = (
    operation: (date: Date, amount: number) => Date,
  ) => {
    setCurrentPeriod((oldVals) => {
      const startOfPeriodDate = operation(oldVals[0], daysAmount);
      return getCalendarDays(startOfPeriodDate);
    });
  };

  const handleNextPeriod = () => handlePeriodChange(addDays);
  const handlePreviousPeriod = () => handlePeriodChange(subDays);

  const handleDateSelection = (newDate: Date) => {
    setCurrentPeriod(getCalendarDays(newDate));
  };

  return (
    <div className={'ml-0 md:ml-8'}>
      <CalendarHeader
        currentPeriod={currentCalendarPeriod}
        onPrevPeriod={handlePreviousPeriod}
        onNextPeriod={handleNextPeriod}
        handleDateSelection={handleDateSelection}
      />
      <Suspense fallback={<LoadingSpinner />}>
        <CalendarBody
          currentPeriod={currentCalendarPeriod}
          disabled={disabled}
        />
      </Suspense>
    </div>
  );
};

export default Calendar;
