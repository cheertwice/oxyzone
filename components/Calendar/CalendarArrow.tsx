import React from 'react';
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io';

interface Props {
  direction: 'prev' | 'next';
  onClick: () => void;
}

const CalendarArrow: React.FC<Props> = ({ direction, onClick }) => (
  <button onClick={onClick}>
    {direction === 'prev' ? <IoIosArrowBack /> : <IoIosArrowForward />}
  </button>
);

export default CalendarArrow;
