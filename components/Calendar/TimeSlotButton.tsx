import React from 'react';
import { FaPlus } from 'react-icons/fa';

interface TimeSlotButtonProps {
  day: Date;
  time: string;
  chamberType: string;
  handleTimeSlotClick: (day: Date, time: string, chamberType: string) => void;
  allPeriodAppointments: {
    id: number;
    date: Date;
    userId: number;
    chamberType: string;
  }[];
  disabled: boolean;
}

const TimeSlotButton: React.FC<TimeSlotButtonProps> = ({
  day,
  time,
  chamberType,
  handleTimeSlotClick,
  allPeriodAppointments,
  disabled,
}) => {
  const [hours, minutes] = time.split(':').map(Number);
  const buttonDate = new Date(day);
  buttonDate.setHours(hours, minutes, 0, 0);

  const now = new Date();
  const isPast = buttonDate.getTime() < now.getTime();
  const isWithin24Hours =
    buttonDate.getTime() <= now.getTime() + 24 * 60 * 60 * 1000;
  const isWeekend = buttonDate.getDay() === 0 || buttonDate.getDay() === 6;

  const shouldBeRed =
    allPeriodAppointments.some((appointment) => {
      return new Date(appointment.date).getTime() === buttonDate.getTime();
    }) || isWeekend;

  const isDisabled = isPast || isWithin24Hours || disabled;

  return (
    <div className="flex items-center justify-center h-60 w-5/6">
      <button
        className={`flex items-center justify-center rounded-lg border-2 w-full h-full text-gray-700 transition duration-300 ease-in-out transform ${
          shouldBeRed
            ? 'bg-red-100 border-red-700 text-red-700 cursor-not-allowed'
            : isDisabled
              ? 'bg-gray-200 border-gray-700 cursor-not-allowed'
              : 'hover:bg-green-500 bg-gray-200 border-gray-700 hover:text-white hover:scale-x-110 active:scale-x-90'
        }`}
        onClick={() => handleTimeSlotClick(day, time, chamberType)}
        style={{ height: '236px' }}
        disabled={shouldBeRed || isDisabled}
      >
        {!shouldBeRed && !isDisabled && <FaPlus className="text-2xl" />}
      </button>
    </div>
  );
};

export default TimeSlotButton;
