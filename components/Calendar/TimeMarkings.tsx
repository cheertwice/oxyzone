import React from 'react';

interface TimeMarkingsProps {
  times: string[];
}

const TimeMarkings: React.FC<TimeMarkingsProps> = ({ times }) => (
  <div className="flex flex-col">
    <div className="p-2 h-12"></div> {/* Placeholder for alignment */}
    <div className="p-2 h-7"></div> {/* Placeholder for alignment */}
    <div className="p-2 h-4"></div> {/* Placeholder for alignment */}
    {times.map((time, index) => (
      <div key={index} className="p-2 h-12 items-center justify-center">
        {time}
      </div>
    ))}
  </div>
);

export default TimeMarkings;
