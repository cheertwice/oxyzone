import React from 'react';
import CalendarDate from './CalendarDate';
import CalendarArrow from './CalendarArrow';
import { format } from 'date-fns';
import { cs } from 'date-fns/locale';

interface Props {
  currentPeriod: Date[];
  onPrevPeriod: () => void;
  onNextPeriod: () => void;
  handleDateSelection: (date: Date) => void;
}

const CalendarHeader: React.FC<Props> = ({
  currentPeriod,
  onPrevPeriod,
  onNextPeriod,
  handleDateSelection,
}) => {
  const periodStart = format(currentPeriod[0], 'd. MMMM', { locale: cs });
  const periodEnd = format(
    currentPeriod[currentPeriod.length - 1],
    'd. MMMM yyyy',
    {
      locale: cs,
    },
  );

  return (
    <div className="flex items-center justify-center mb-10 md:mt-0 mt-2 gap-x-4 md:gap-x-6 lg:gap-x-8 xl:gap-x-10">
      <CalendarArrow direction="prev" onClick={onPrevPeriod} />
      <CalendarDate
        date={`${periodStart} - ${periodEnd}`}
        onSelect={handleDateSelection}
      />
      <CalendarArrow direction="next" onClick={onNextPeriod} />
    </div>
  );
};

export default CalendarHeader;
