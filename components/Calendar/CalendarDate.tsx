'use client';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import React from 'react';
import { cs } from 'date-fns/locale';

interface Props {
  date: string;
  onSelect: (date: Date) => void;
}

const CalendarDate: React.FC<Props> = ({ date, onSelect }) => {
  const [showDatePicker, setShowDatePicker] = React.useState(false);

  return (
    <div className="w-80 text-center">
      <p
        className={'hover:cursor-pointer select-none'}
        onClick={() =>
          showDatePicker ? setShowDatePicker(false) : setShowDatePicker(true)
        }
      >
        {date}
      </p>
      {showDatePicker && (
        <div className="absolute z-50 mt-5 ml-11">
          <DatePicker
            selected={new Date()}
            onChange={(date: Date) => {
              onSelect(date);
              setShowDatePicker(false);
            }}
            inline
            locale={cs}
          />
        </div>
      )}
    </div>
  );
};

export default CalendarDate;
