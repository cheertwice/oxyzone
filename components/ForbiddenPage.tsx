import React from 'react';

const ForbiddenPage = () => {
  return (
    <div className="min-h-screen bg-white flex flex-col justify-center items-center pt-6 sm:pt-0 mx-auto">
      <h2 className="text-2xl font-semibold text-center text-gray-800 mb-6">
        Přístup na tuto stránku je zakázán
      </h2>
      <p className="text-gray-500 text-center">
        Tato stránka je určena pouze pro oprávněné uživatele.
      </p>
    </div>
  );
};

export default ForbiddenPage;
