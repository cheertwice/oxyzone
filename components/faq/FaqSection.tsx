import React from 'react';

const FaqSection = () => {
  const faqItems = [
    {
      heading: 'Co je hyperbarická kyslíková terapie (dále jen HBOT)?',
      text: 'Podstatou této metody je vdechování kyslíku při vyšším tlaku než je tlak atmosférický a ve vyšší koncentraci (téměř 100%) pomocí tzv. kyslíkových brýlích v přetlakové komoře.',
    },
    {
      heading: 'Jak procedura HBOT probíhá?',
      text: 'Klient se pohodlně usadí či uloží do hyperbarické komory a nasadí si kyslíkovou masku nebo kyslíkové brýle, přes které začne vdechovat téměř 100% kyslík. Obsluha následně komoru hermeticky uzavře a pomocí ventilu se komora začne pomalu tlakovat. Pocit při tlakování je podobný jako v letadle při jeho stoupání - tlak v uších či jejich zaléhání. Tohoto pocitu se lze zbavit žvýkáním či zíváním. Při tlaku cca 1,3 atmosféry dojde k vyrovnání tlaku v uších a pocit zalehlých uší odeznívá. Klient může následně během procedury odpočívat, spát, číst si, nebo pracovat na telefonu či notebooku. Na konci terapie obsluha pozvolna komoru odtlakuje, otevře a pacient může komoru opustit.',
    },
    {
      heading: 'Je HBOT bezpečná?',
      text: 'Procedura HBOT je příjemná, bezbolestná a pomáhá v širokém spektru diagnóz viz sekce Indikace. Ačkoli existují malá rizika jako u všech léčebných postupů, celková hyperbarická kyslíková terapie je při správném postupu velmi bezpečná.',
    },
    {
      heading: 'Existují nějaké vedlejší účinky HBOT?',
      text: 'Procedura HBOT je příjemná a bezbolestná. Nejběžnějším vedlejším účinkem je barotrauma uší. Při tlakování komory má pacient podobný pocit, jako při startu letadla a stoupání do letové hladiny. V případě zaléhání uší postupujte jako v letadle – předsuňte spodní čelist dopředu, případně zkuste žvýkat či zívat. Takto dojde k okamžitému uvolnění mírného tlaku v uších. Tyto problémy po dosažení terapeutické hladiny tlaku samovolně a rychle odezní. Vzácným vedlejším účinkem může být toxicita kyslíku, která je způsobena podáním příliš velkého množství kyslíku. Tento stav by při běžné 1-2 hodinové terapii neměl nastat. Běžně se udává, že první příznaky toxicity kyslíku přicházejí po 12-16ti hodinovém vdechování 100% kyslíku. Přestože je HBOT bezpečná procedura, existují kontraindikace, při kterých se terapie nedoporučují (např. těhotenství, silné astma, klaustrofobie apod). Kompletní přehled těchto výjimek naleznete v sekci Kontraindikace.',
    },
    {
      heading: 'Jak by se pacienti měli připravit na pobyt v komoře?',
      text: 'Do komory by měl pacient vstoupit v čistém bavlněném oblečení. Nesmí být pod vlivem alkoholu nebo jiných návykových látek a neměl by krátce před vstupem pít ve velké míře nápoje sycené oxidem uhličitým. V komoře nesmí pacienti kouřit ani pít alkoholické nápoje. V komoře je možné relaxovat i pracovat. Pacienti si mohou vzít dovnitř knihu, mobilní telefon, notebook či tablet.',
    },
    {
      heading: 'Existují nějaké zákazy či omezení pro užívání HBOT?',
      text: 'Při proceduře v HBOT je zakázáno kouřit, zapalovat svíčky či jinak pracovat s ohněm. Při vlastní terapii je vdechován 100% kyslík a ten je vysoce hořlavý. Hyperbarickou komoru je zakázáno jakkoliv přestavovat, demontovat nebo ničit. Rozhodně je zakázáno používat jakékoliv ostré předměty či neodborná nářadí na komoru či její příslušenství. Pro jakékoliv potřeby opravy či úpravy, kontaktujte prosím nejdříve naši společnost k vyřešení situace. Veškeré vybavení hyperbarické komory udržujte mimo dosah malých dětí, aby nedošlo k poškození, nebo poničení jednotlivých částí komory.',
    },
  ];

  return (
    <div className="py-12 px-4 sm:px-6 lg:px-8 bg-gray-50">
      <div className="max-w-4xl mx-auto">
        <div className="mb-10 text-center">
          <h2 className="text-3xl font-semibold text-gray-800">
            Často kladené otázky
          </h2>
        </div>
        <div className="space-y-4">
          {faqItems.map((item, index) => (
            <div
              key={index}
              className="bg-white shadow overflow-hidden rounded-md p-5"
            >
              <h4 className="text-xl font-semibold text-gray-800 mb-2">
                {item.heading}
              </h4>
              <p className="text-gray-600">{item.text}</p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default FaqSection;
