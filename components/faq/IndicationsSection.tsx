const IndicationsSection = () => {
  return (
    <div className="py-12 px-4 md:px-8">
      <div className="max-w-7xl mx-auto">
        <div className="mb-8 text-center">
          <h2 className="text-3xl font-semibold">Indikace</h2>
          <p className="italic">HBOT pomáhá u těchto onemocnění:</p>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-3 gap-8">
          {/* Column 1 */}
          <div className="space-y-4">
            <h3 className="font-bold">Autoimunitní onemocnění</h3>
            <ul className="list-disc pl-5">
              <li>Alergická rýma</li>
              <li>Autoimunitní hepatitida</li>
              <li>Diabetes mellitus I a II typu</li>
              <li>Revmatoidní artritida</li>
              <li>Roztroušená skleróza</li>
            </ul>
            <h3 className="font-bold">Dětská onemocnění</h3>
            <ul className="list-disc pl-5">
              <li>Autismus</li>
              <li>Epilepsie</li>
            </ul>
            <h3 className="font-bold">Kardiovaskulární onemocnění</h3>
            <ul className="list-disc pl-5">
              <li>Prevence a brzká rekonvalescence:</li>
              <li>Cévní mozková příhoda</li>
              <li>Infarkt myokardu</li>
            </ul>
          </div>
          {/* Column 2 */}
          <div className="space-y-4">
            <h3 className="font-bold">Infekční onemocnění</h3>
            <ul className="list-disc pl-5">
              <li>Lymská borelióza</li>
            </ul>
            <h3 className="font-bold">Kožní onemocnění</h3>
            <ul className="list-disc pl-5">
              <li>Atopický ekzém</li>
              <li>Bércový vřed</li>
              <li>Lupénka</li>
              <li>Nehojící se rány</li>
              <li>Pásový opar</li>
              <li>Svrab</li>
            </ul>
            <h3 className="font-bold">Regenerace pohybového aparátu</h3>
            <ul className="list-disc pl-5">
              <li>
                Úrazy, rehabilitace po operacích a chirurgických zákrocích
              </li>
              <li>Sportovní regenerace</li>
            </ul>
          </div>
          {/* Column 3 */}
          <div className="space-y-4">
            <h3 className="font-bold">Neurologická onemocnění</h3>
            <ul className="list-disc pl-5">
              <li>Alzheimerova choroba</li>
              <li>Migréna a bolesti hlavy</li>
              <li>Parkinsonova choroba</li>
              <li>Stimulace a regenerace mozkových buněk</li>
            </ul>
            <h3 className="font-bold">Ostatní onemocnění</h3>
            <ul className="list-disc pl-5">
              <li>Celulitida</li>
              <li>Dekompresní nemoc</li>
              <li>Erektilní dysfunkce</li>
              <li>Chronický únavový syndrom</li>
              <li>Náhlá hluchota</li>
              <li>Otrava oxidem uhelnatým</li>
              <li>Traumatická poranění orgánů</li>
              <li>Crohnova choroba, chronické záněty střeva</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default IndicationsSection;
