const ContraIndicationsSection = () => {
  return (
    <div className="py-12 px-4 md:px-8 bg-gray-50">
      <div className="max-w-7xl mx-auto">
        <div className="mb-8 text-center">
          <h2 className="text-3xl font-semibold">Kontraindikace</h2>
          <p className="italic">HBOT procedury se nedoporučují:</p>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
          {/* Column 1 */}
          <div className="space-y-4">
            <ul className="list-disc pl-5">
              <li>Těhotným ženám</li>
              <li>Lidem s neošetřeným pneumotoraxem</li>
              <li>Lidem s těžkým astmatem bronchiale</li>
              <li>Lidem s problémy s Eustachovou trubicí</li>
              <li>
                Lidem s kardiostimulátorem doporučujeme konzultaci se svým
                kardiologem.
              </li>
            </ul>
          </div>
          {/* Column 2 */}
          <div className="space-y-4">
            <ul className="list-disc pl-5">
              <li>Lidem s neléčeným vysokým tlakem</li>
              <li>Lidem s klaustrofobií</li>
              <li>Lidem s frakturou lebeční kosti</li>
              <li>
                Děti ve věku do 15 let smí podstupovat procedury jen v doprovodu
                dospělé osoby, která bude přítomna s dítětem uvnitř komory.
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContraIndicationsSection;
