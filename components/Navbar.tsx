'use client';
import Link from 'next/link';
import Image from 'next/image';
import logo from '../public/static/images/oxyzone-logo.svg';
import { ACCOUNT_LINKS, ADMIN_LINKS, NAV_LINKS } from '@/constants';
import {
  Button,
  Divider,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  Menu,
  MenuButton,
  MenuDivider,
  MenuGroup,
  MenuItem,
  MenuList,
  useDisclosure,
} from '@chakra-ui/react';
import React from 'react';
import { FaBars, FaTimes } from 'react-icons/fa';
import { signOut, useSession } from 'next-auth/react';
import { FaChevronDown } from 'react-icons/fa';
import { CiLogout } from 'react-icons/ci';
import { showToast } from '@/lib/toastUtils';
import { usePathname } from 'next/navigation';

const Navbar = () => {
  const pagePath = usePathname();
  const { data: session } = useSession();

  const {
    isOpen: isDrawerOpen,
    onOpen: onDrawerOpen,
    onClose: onDrawerClose,
  } = useDisclosure();

  const handleLogout = (): void => {
    signOut({ callbackUrl: '/', redirect: true }).then(() => {
      showToast('Úspěšně odhlášeno', 'success');
    });
  };

  return (
    <div className="flex justify-between items-center w-full h-20 px-4 text-black bg-white nav top-0 sticky z-50">
      {/*Logo*/}
      <div className="flex">
        <Link href="/" className={`flex items-center text-3xl`}>
          <Image
            className="mr-3"
            src={logo}
            alt="Firemní logo"
            width={50}
            height={50}
          />
          OXYZONE
        </Link>
      </div>

      <ul className="hidden md:flex items-center">
        {NAV_LINKS.map(({ key, href, label }) => (
          <li
            key={key}
            className={`nav-links px-4 cursor-pointer capitalize text-gray-500 hover:scale-105 duration-200 link-underline hover:font-bold ${href === pagePath ? 'font-extrabold' : 'font-medium'}`}
          >
            {key === 'login' ? (
              session ? (
                <Menu>
                  <MenuButton as={Button} rightIcon={<FaChevronDown />}>
                    Uživatelský panel
                  </MenuButton>
                  <MenuList>
                    {ACCOUNT_LINKS.map((link) => (
                      <MenuItem
                        as="a"
                        icon={<link.icon />}
                        href={link.href}
                        key={link.href}
                        className={`${link.href === pagePath ? 'font-bold' : ''}`}
                      >
                        {link.name}
                      </MenuItem>
                    ))}
                    <MenuDivider></MenuDivider>
                    {session?.user.isAdmin && (
                      <MenuGroup title={'Administrace'}>
                        {ADMIN_LINKS.map((link) => (
                          <MenuItem
                            as="a"
                            icon={<link.icon />}
                            href={link.href}
                            key={link.href}
                            className={`${link.href === pagePath ? 'font-bold' : ''}`}
                          >
                            {link.name}
                          </MenuItem>
                        ))}
                      </MenuGroup>
                    )}

                    {/*<MenuItem*/}
                    {/*  as="a"*/}
                    {/*  icon={<CiLogout />}*/}
                    {/*  key="logout"*/}
                    {/*  onClick={handleLogout}*/}
                    {/*>*/}
                    {/*  Odhlásit se*/}
                    {/*</MenuItem>*/}
                  </MenuList>
                </Menu>
              ) : (
                <Link href={href}> {label}</Link>
              )
            ) : (
              <Link href={href}>{label}</Link>
            )}
          </li>
        ))}
      </ul>

      <div
        onClick={isDrawerOpen ? onDrawerClose : onDrawerOpen}
        className="cursor-pointer pr-4 z-10 text-gray-500 md:hidden"
      >
        {isDrawerOpen ? <FaTimes size={30} /> : <FaBars size={30} />}
      </div>

      <Drawer
        isOpen={isDrawerOpen}
        placement="right"
        onClose={onDrawerClose}
        size="xs"
      >
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton className={'items-center'} />
          <DrawerBody>
            <div className={'mt-5 mb-4'}>
              <p className={'font-bold'}>Stránky</p>
              <Divider
                className={'mt-2'}
                borderWidth={'1px'}
                borderColor={'blue.200'}
                orientation={'horizontal'}
              ></Divider>
            </div>
            <ul className={'space-y-4'}>
              {NAV_LINKS.map(({ key, href, label }) => (
                <li key={key} className={''}>
                  {key === 'login' ? (
                    !session && (
                      <Link onClick={onDrawerClose} href={href}>
                        {label}
                      </Link>
                    )
                  ) : (
                    <Link onClick={onDrawerClose} href={href}>
                      {label}
                    </Link>
                  )}
                </li>
              ))}
            </ul>
            {session && (
              <div className={'mt-6 mb-4'}>
                <p className={'font-bold'}>Uživatelský panel</p>
                <Divider
                  className={'mt-2'}
                  borderWidth={'1px'}
                  borderColor={'blue.200'}
                  orientation={'horizontal'}
                ></Divider>
              </div>
            )}

            {session && (
              <ul className={'space-y-4'}>
                {ACCOUNT_LINKS.map((link) => (
                  <li key={link.href} className={'flex items-center space-x-2'}>
                    <link.icon />
                    <Link onClick={onDrawerClose} href={link.href}>
                      {link.name}
                    </Link>
                  </li>
                ))}
              </ul>
            )}

            {session?.user.isAdmin && (
              <div className={'mt-6 mb-4'}>
                <p className={'font-bold'}>Administrace</p>
                <Divider
                  className={'mt-2'}
                  borderWidth={'1px'}
                  borderColor={'blue.200'}
                  orientation={'horizontal'}
                ></Divider>
              </div>
            )}

            {session?.user.isAdmin && (
              <ul className={'space-y-4'}>
                {ADMIN_LINKS.map((link) => (
                  <li key={link.href} className={'flex items-center space-x-2'}>
                    <link.icon />
                    <Link onClick={onDrawerClose} href={link.href}>
                      {link.name}
                    </Link>
                  </li>
                ))}
              </ul>
            )}
            {/*<Divider*/}
            {/*  className={'mt-2'}*/}
            {/*  borderWidth={'1px'}*/}
            {/*  borderColor={'blue.200'}*/}
            {/*  orientation={'horizontal'}*/}
            {/*></Divider>*/}
            {/*<li key={'logout'} className={'flex items-center space-x-2 mt-4'}>*/}
            {/*  <CiLogout />*/}
            {/*  <Link onClick={handleLogout} href={'/'}>*/}
            {/*    Odhlásit se*/}
            {/*  </Link>*/}
            {/*</li>*/}
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </div>
  );
};

export default Navbar;
