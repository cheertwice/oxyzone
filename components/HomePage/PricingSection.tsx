import React from 'react';

const PricingSection = React.forwardRef<HTMLDivElement, any>((props, ref) => {
  const services = [
    { title: 'Jednorázová terapie', price: '750 Kč' },
    { title: 'Permanentka 5 vstupů', price: '3500 Kč' },
    { title: 'Permanentka 10 vstupů', price: '6500 Kč' },
  ];

  return (
    <div className="bg-gray-50 py-12 md:py-16 px-4 md:px-8" id={'pricing'}>
      <div className="grid grid-cols-1 md:grid-cols-2 md:gap-x-10">
        <div className="flex flex-col space-y-8 max-w-md mx-auto">
          <h5 className="text-xl text-center sm:text-left">
            První terapii u nás máte <b>zdarma</b>.
          </h5>
          <p className="text-base text-justify">
            Celkový čas jedné procedury je 60 min. V ceně je zahrnut příchod,
            vstup do komory, tlakování komory, pobyt v komoře, odtlakování
            komory, odchod. Před vstupem do komory je klientovi vysvětlen postup
            a pravidla procedury.
          </p>
        </div>

        <div className="mt-10 md:mt-0 max-w-md xl:max-w-2xl">
          <h4 className="text-2xl mb-4 text-center md:text-left">Ceník</h4>
          <div className="">
            <ul className="space-y-2.5 w-full">
              {services.map((service, index) => (
                <li key={index} className="flex">
                  <h5 className="flex-shrink-0">{service.title}</h5>
                  <div
                    className="flex-grow bg-gray-600 mx-3 my-4"
                    style={{ height: '1px' }}
                  ></div>
                  <p className="font-bold flex-shrink-0 text-blue-500">
                    {service.price}
                  </p>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
});

PricingSection.displayName = 'PricingSection';

export default PricingSection;
