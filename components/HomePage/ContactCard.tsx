import React, { ReactNode } from 'react';

interface ContactCardProps {
  iconSrc: string;
  title: string;
  details: ReactNode | string;
}

const ContactCard: React.FC<ContactCardProps> = ({
  iconSrc,
  title,
  details,
}) => {
  return (
    <div className="flex flex-col items-center p-6 shadow-lg rounded-2xl max-w-sm bg-gray-50">
      <img
        src={iconSrc}
        alt={title}
        className="h-16 w-16 mb-4"
        style={{
          filter:
            'brightness(0) saturate(100%) invert(22%) sepia(64%) saturate(4222%) hue-rotate(217deg) brightness(89%) contrast(90%)',
        }}
      />
      <h3 className="text-xl font-semibold mb-2">{title}</h3>

      {title === 'Telefon' ? (
        <div className="text-right text-sm whitespace-nowrap overflow-x-auto">
          {details}
        </div>
      ) : (
        <div className="text-center text-md">{details}</div>
      )}
    </div>
  );
};

export default ContactCard;
