import React from 'react';
import ContactCard from './ContactCard';

const contacts = [
  {
    title: 'E-mail',
    details: <a href="mailto:info@oxyzone.cz">info@oxyzone.cz</a>,
    iconSrc: '/static/images/email_icon.svg',
  },
  {
    title: 'Telefon',
    details: (
      <>
        Jana Smetanová: <a href="tel:+420602569967">+420602569967</a>
        <br />
        <span className="inline-block">
          <em>for english call</em> Ondřej Šedivý:{' '}
          <a href="tel:+420739582224">+420739582224</a>
        </span>
      </>
    ),
    iconSrc: '/static/images/phone_icon.svg',
  },
  {
    title: 'Adresa',
    details: (
      <a
        href="https://www.google.com/maps/place/Oxyzone+-+hyperbarick%C3%A1+oxygenoterapie/@49.9937426,14.6580964,19z/data=!3m1!4b1!4m6!3m5!1s0x470b89000c02e025:0x883e3eb33051a14d!8m2!3d49.9937417!4d14.6587401!16s%2Fg%2F11vq7jyg_n?entry=ttu"
        target="_blank"
        rel="noopener noreferrer"
      >
        <span className="inline-block">17. listopadu 700/22</span>
        <span className="inline-block">251 01, Říčany u Prahy</span>
      </a>
    ),
    iconSrc: '/static/images/address_icon.svg',
  },
];

const ContactSection = () => {
  return (
    <div className="py-10 px-5 max-w-7xl mx-auto mt-10 mb-10">
      <h2 className="text-center mb-10 text-3xl font-bold text-gray-900">
        Kontakty
      </h2>
      <div
        className="grid grid-cols-1 xl:grid-cols-3 md:grid-cols-2 items-center"
        style={{ gap: '80px' }}
      >
        {contacts.map((contact, index) => (
          <ContactCard
            key={index}
            title={contact.title}
            details={contact.details}
            iconSrc={contact.iconSrc}
          />
        ))}
      </div>
    </div>
  );
};

export default ContactSection;
