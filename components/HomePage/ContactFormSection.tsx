import React, { FormEvent, useState } from 'react';
import { showToast } from '@/lib/toastUtils';

const ContactFormSection = () => {
  const [name, setName] = useState('');
  const [contact, setContact] = useState('');
  const [message, setMessage] = useState('');
  const [loading, setLoading] = useState(false);

  const handleFormSubmit = async (e: FormEvent) => {
    e.preventDefault();
    setLoading(true);

    const response = await fetch('/api/contact-form', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: name,
        contact: contact,
        message: message,
      }),
    });

    setLoading(false);

    if (response.ok) {
      showToast(
        'Váš dotaz jsme přijali. Ozveme se ihned, jakmile to bude možné',
        'success',
      );
      setName('');
      setContact('');
      setMessage('');
    } else {
      showToast('Chyba při odesílání dotazníku', 'error');
    }
  };

  return (
    <div className={'bg-gray-50'}>
      <div className="py-16 px-5 max-w-7xl mx-auto mt-10">
        <h2 className="text-center mb-10 text-3xl font-bold text-gray-900">
          Kontaktujte nás
        </h2>
        <div className="grid grid-cols-1 lg:grid-cols-2 gap-12">
          {/* Contact Form */}
          <div>
            <form className="space-y-6" onSubmit={handleFormSubmit}>
              <div>
                <label
                  htmlFor="name"
                  className="block text-sm font-medium text-gray-700"
                >
                  Vaše jméno
                </label>
                <div className="mt-1">
                  <input
                    type="text"
                    id="name"
                    name="name"
                    required
                    className="block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    placeholder="Zadejte Vaše jméno"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    disabled={loading}
                  />
                </div>
              </div>
              <div>
                <label
                  htmlFor="contact"
                  className="block text-sm font-medium text-gray-700"
                >
                  E-mail nebo tel.číslo
                </label>
                <div className="mt-1">
                  <input
                    type="text"
                    id="contact"
                    name="contact"
                    required
                    className="block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    placeholder="Zadejte Váš e-mail nebo tel. číslo"
                    value={contact}
                    onChange={(e) => setContact(e.target.value)}
                    disabled={loading}
                  />
                </div>
              </div>
              <div>
                <label
                  htmlFor="message"
                  className="block text-sm font-medium text-gray-700"
                >
                  Zpráva
                </label>
                <div className="mt-1">
                  <textarea
                    id="message"
                    name="message"
                    rows={4}
                    required
                    className="block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    placeholder="Napište nám svou zprávu"
                    value={message}
                    onChange={(e) => setMessage(e.target.value)}
                    disabled={loading}
                  ></textarea>
                </div>
              </div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className={`inline-flex items-center px-6 py-3 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ${
                    loading ? 'opacity-50 cursor-not-allowed' : ''
                  }`}
                  disabled={loading}
                >
                  {loading ? 'Odesílání...' : 'Odeslat zprávu'}
                </button>
              </div>
            </form>
          </div>

          {/* Google Map */}
          <div>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2564.233982093672!2d14.65809641570923!3d49.993741679396516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470b89000c02e025%3A0x883e3eb33051a14d!2s17.%20listopadu%20700%2F22%2C%20251%2001%20%C5%98%C3%AD%C4%8Dany%20u%20Prahy!5e0!3m2!1sen!2scz!4v1693994992974!5m2!1sen!2scz"
              width="100%"
              height="450"
              style={{ border: 0 }}
              allowFullScreen={false}
              loading="lazy"
              className="shadow-lg rounded-lg"
            ></iframe>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactFormSection;
