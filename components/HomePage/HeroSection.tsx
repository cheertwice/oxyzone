import Image from 'next/image';
const HeroSection = () => {
  return (
    <div className="bg-white text-gray-900 py-10">
      <div className="max-w-7xl mx-auto px-4 md:px-6 lg:px-8">
        <div className="flex flex-wrap -mx-4">
          <div className="w-full lg:w-1/2 px-4 mb-10 lg:mb-0">
            <div className="flex h-full flex-col justify-center">
              <div className="flex flex-col space-y-5">
                <h6 className="text-2xl text-center italic text-gray-700 font-normal">
                  &quot;Kyslík do každé buňky&quot;
                </h6>
                <p className="font-bold text-center">První terapie zdarma!</p>
                <p className="text-justify">
                  Hyperbarická oxygenoterapie (HBOT) je metoda spočívající v
                  inhalaci téměř 100% kyslíku za podmínek tlaku vyššího, než je
                  tlak atmosférický. Zvýšeného tlaku lze dosáhnout pouze v
                  uzavřeném zařízení, které se nazývá hyperbarická (přetlaková)
                  komora. Terapie pomáhá v širokém spektru diagnóz. Podrobné
                  informace najdete v sekci FAQ.
                </p>
                <div className="flex justify-center space-x-6">
                  <a
                    href="tel:+420602569967"
                    target="_blank"
                    className="text-center px-4 py-2 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700"
                  >
                    Telefonicky objednat
                  </a>
                  <a
                    href="/faq"
                    className="text-center px-4 py-2 border border-gray-300 shadow-sm text-base font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
                  >
                    Další informace
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div className="w-full lg:w-1/2 px-6 ">
            <img
              src="/static/images/shop.jpg"
              alt="Interior view of our facility showcasing hyperbaric chambers for customer therapy sessions"
              className="w-full h-auto object-cover shadow-lg rounded-lg"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeroSection;
