// components/AppointmentHistory.tsx
import React from 'react';

interface Appointment {
  id: string;
  date: string; // ISO string format
}

interface AppointmentHistoryProps {
  appointments: Appointment[];
}

const AppointmentHistory: React.FC<AppointmentHistoryProps> = ({
  appointments,
}) => {
  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4 p-4">
      {appointments.map((appointment) => (
        <div key={appointment.id} className="p-4 bg-white shadow-md rounded-lg">
          <span className="text-gray-800">
            {new Date(appointment.date).toLocaleDateString()}
          </span>
        </div>
      ))}
    </div>
  );
};

export default AppointmentHistory;
