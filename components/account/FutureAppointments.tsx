// components/FutureAppointments.tsx
import { AiFillEdit } from 'react-icons/ai'; // Edit icon
import { MdCancel } from 'react-icons/md';
import React from 'react'; // Cancel icon

interface Appointment {
  id: string;
  date: string; // ISO string format
}

interface FutureAppointmentsProps {
  appointments: Appointment[];
  onCancel: (id: string) => void;
  onEdit: (id: string) => void;
}

const FutureAppointments: React.FC<FutureAppointmentsProps> = ({
  appointments,
  onCancel,
  onEdit,
}) => {
  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4 p-4">
      {appointments.map((appointment) => (
        <div
          key={appointment.id}
          className="p-4 bg-white shadow-md rounded-lg flex justify-between items-center"
        >
          <span className="text-gray-800">
            {new Date(appointment.date).toLocaleDateString()}
          </span>
          <div>
            <button
              className="text-blue-500 hover:text-blue-700 mr-2"
              onClick={() => onEdit(appointment.id)}
            >
              <AiFillEdit />
            </button>
            <button
              className="text-gray-500 hover:text-gray-700"
              onClick={() => onCancel(appointment.id)}
            >
              <MdCancel />
            </button>
          </div>
        </div>
      ))}
    </div>
  );
};

export default FutureAppointments;
