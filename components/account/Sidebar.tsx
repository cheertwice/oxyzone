'use client';
import React from 'react';
import Link from 'next/link';
import { ACCOUNT_LINKS, ADMIN_LINKS } from '@/constants';
import { usePathname } from 'next/navigation';
import { Divider } from '@chakra-ui/react';
import { useSession } from 'next-auth/react';

const Sidebar: React.FC = () => {
  const pagePath = usePathname();
  const { data: session } = useSession();

  return (
    <aside className="w-1/6 min-w-60 min-h-screen bg-white p-5 space-y-4 hidden md:block sticky top-0 border-r-2 border-blue-200">
      <ul className={'space-y-2'}>
        <li className="p-2 text-gray-700 rounded-md font-bold">
          Uživatelský panel
        </li>
        <Divider
          orientation="horizontal"
          variant="solid"
          borderWidth="1px"
          className="mb-3"
          borderColor="blue.200"
        />
        {ACCOUNT_LINKS.map((link) => (
          <li key={link.name} className="group">
            <Link
              href={link.href}
              className={`flex items-center p-2 text-gray-700 hover:bg-blue-500 hover:text-white rounded-md transition-colors duration-200 ${link.href === pagePath ? 'bg-blue-500 text-white' : ''}`}
              aria-current={link.href === pagePath ? 'page' : undefined}
            >
              <link.icon className="mr-3" />
              {link.name}
            </Link>
          </li>
        ))}
      </ul>
      {session?.user.isAdmin && (
        <ul className={'space-y-2'}>
          <li className="p-2 text-gray-700 rounded-md font-bold">
            Administrace
          </li>
          <Divider
            orientation="horizontal"
            variant="solid"
            borderWidth="1px"
            className="mb-3"
            borderColor="blue.200"
          />
          {ADMIN_LINKS.map((link) => (
            <li key={link.name} className="group">
              <Link
                href={link.href}
                className={`flex items-center p-2 text-gray-700 hover:bg-blue-500 hover:text-white rounded-md transition-colors duration-200 ${link.href === pagePath ? 'bg-blue-500 text-white' : ''}`}
                aria-current={link.href === pagePath ? 'page' : undefined}
              >
                <link.icon className="mr-3" />
                {link.name}
              </Link>
            </li>
          ))}
        </ul>
      )}
    </aside>
  );
};

export default Sidebar;
