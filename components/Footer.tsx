import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import logo from '../public/static/images/oxyzone-logo.svg';

const Footer: React.FC = () => {
  return (
    <footer className="text-gray-600 body-font bg-gray-50">
      <div className="container px-5 py-8 mx-auto flex items-center lg:flex-row flex-col">
        <Link href="/" passHref>
          <div className="flex title-font font-medium items-center lg:justify-start justify-center text-gray-900 cursor-pointer">
            <Image src={logo} alt="OXYZONE logo" width={50} height={50} />
            <span className="ml-3 text-xl">OXYZONE</span>
          </div>
        </Link>
        <p className="text-sm text-gray-500 sm:ml-4 sm:pl-4 lg:border-l-2 sm:border-gray-200 sm:py-2 sm:mt-0 mt-4">
          © {new Date().getFullYear()} OXYZONE
        </p>
        <span className="inline-flex sm:ml-auto sm:mt-0 mt-4 justify-center sm:justify-start">
          <a href="tel:+420602569967" className="text-gray-500">
            +420602569967
          </a>
          <a href="tel:+420739582224" className="ml-4 text-gray-500">
            +420739582224
          </a>
          <a href="mailto:info@oxyzone.cz" className="ml-4 text-gray-500">
            info@oxyzone.cz
          </a>
        </span>
        <div className="sm:ml-4 sm:pl-4 sm:py-2 sm:mt-0 mt-4">
          <a
            href="https://www.google.com/maps/place/Oxyzone+-+hyperbarick%C3%A1+oxygenoterapie/@49.9937426,14.6580964,19z/data=!3m1!4b1!4m6!3m5!1s0x470b89000c02e025:0x883e3eb33051a14d!8m2!3d49.9937417!4d14.6587401!16s%2Fg%2F11vq7jyg_n?entry=ttu"
            target="_blank"
            rel="noopener noreferrer"
          >
            <p className="text-sm text-gray-500">
              17. listopadu 700/22, 251 01 Říčany
            </p>
          </a>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
