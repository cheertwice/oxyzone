import { withAuth } from 'next-auth/middleware';
import { NextResponse } from 'next/server';
import { getToken } from 'next-auth/jwt';

export default withAuth(async function middleware(req) {
  const token = await getToken({ req });

  if (req.nextUrl.pathname.startsWith('/api/admin')) {
    if (!token?.isAdmin) {
      return NextResponse.json(
        { success: false, error: 'Přístup odepřen' },
        { status: 403 },
      );
    }
  }

  return NextResponse.next();
});

export const config = {
  matcher: ['/account/:path*', '/api/admin/:path*'], // Add '/api/admin/:path*' to matcher
};
