import type { Config } from 'tailwindcss';

const config: Config = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      gridTemplateColumns: {
        // Define a 15-column grid
        '15': 'repeat(15, minmax(0, 1fr))',
      },
      colors: {
        primary: '#7AACD6',
      },
    },
  },
  plugins: [],
};
export default config;
