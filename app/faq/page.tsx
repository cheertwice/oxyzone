import React from 'react';
import IndicationsSection from '@/components/faq/IndicationsSection';
import ContraIndicationsSection from '@/components/faq/ContraIndicationsSection';
import FaqSection from '@/components/faq/FaqSection';

function Page() {
  return (
    <div className="App">
      <IndicationsSection />
      <ContraIndicationsSection />
      <FaqSection />
    </div>
  );
}

export default Page;
