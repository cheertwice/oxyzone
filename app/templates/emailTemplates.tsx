export const confirmationEmailTemplate = (date: string): string => `
<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <title>Rezervace Potvrzena</title>
    <style>
        body { font-family: Arial, sans-serif; background-color: #f4f4f4; color: #333; }
        .container { background-color: #ffffff; max-width: 600px; margin: 0 auto; padding: 20px; }
        h1 { color: #0066cc; }
        p { line-height: 1.6; }
        .footer { margin-top: 20px; font-size: 0.9em; text-align: center; color: #666; }
    </style>
</head>
<body>
    <div class="container">
        <h1>Potvrzení Vaší Rezervace</h1>
        <p>Potvrzujeme vaši rezervaci na <strong>${date}</strong>. Detaily uvidíte po přihlášení na <a href=https://oxyzone.cz">našem webu</a>, kde můžete také rezervace spravovat.</p>
        <div class="footer">
            Děkujeme za vaši důvěru,<br>Váš Tým OXYZONE
        </div>
    </div>
</body>
</html>
`;
