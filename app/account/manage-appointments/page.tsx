'use client';
import { useEffect, useState } from 'react';
import { RiDeleteBin6Line } from 'react-icons/ri';
import { FaBed, FaChair } from 'react-icons/fa';
import { showToast } from '@/lib/toastUtils';
import { useSession } from 'next-auth/react';
import ForbiddenPage from '@/components/ForbiddenPage';

interface AdminAppointment {
  id: number;
  date: string;
  userId: number;
  chamberType: 'lay' | 'sit';
  user: {
    firstName: string;
    lastName: string;
    email: string;
  };
}

const AdminAppointments = () => {
  const [appointments, setAppointments] = useState<AdminAppointment[]>([]);
  const { data: session } = useSession();

  const fetchAppointments = async () => {
    const response = await fetch(`/api/admin/all-appointments`);
    if (response.ok) {
      const data = await response.json();
      setAppointments(
        data.appointments.sort(
          (a: AdminAppointment, b: AdminAppointment) =>
            new Date(b.date).getTime() - new Date(a.date).getTime(),
        ),
      );
    } else {
      showToast('Error loading appointments', 'error');
    }
  };

  useEffect(() => {
    fetchAppointments();
  }, []);

  const cancelAppointment = async (
    id: number,
    firstName: string,
    lastName: string,
    email: string,
  ) => {
    const response = await fetch('/api/admin/cancel-appointment', {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ id }),
    });

    if (response.ok) {
      fetchAppointments();
      showToast(
        'Termín úspěšně zrušen',
        'success',
        `Uživatel: ${firstName} ${lastName}, email: ${email}`,
      );
    } else {
      const result = await response.json();
      showToast('Error cancelling appointment', 'error', result.error);
    }
  };

  if (!session || !session.user.isAdmin) {
    return <ForbiddenPage />;
  }

  return (
    <div className="min-h-screen bg-white flex flex-col pt-6 sm:pt-0 mx-auto">
      <div className="w-full px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg max-w-4xl">
        <h2 className="text-2xl font-semibold text-center text-gray-800 mb-6">
          Všechny termíny
        </h2>
        <div className="flex flex-col">
          <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
              <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table className="min-w-full divide-y divide-gray-200">
                  <thead className="bg-gray-50">
                    <tr>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        Jméno
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        Datum a čas
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        Typ komory
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        Akce
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        Action
                      </th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200">
                    {appointments.map((appointment) => (
                      <tr key={appointment.id}>
                        <td className="px-6 py-4 whitespace-nowrap">
                          {appointment.user.firstName}{' '}
                          {appointment.user.lastName}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap">
                          {new Date(appointment.date).toLocaleString('cs-CZ')}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap">
                          {appointment.chamberType === 'lay' ? (
                            <FaBed className="h-6 w-6 text-blue-500 ml-5" />
                          ) : (
                            <FaChair className="h-6 w-6 text-blue-500 ml-5" />
                          )}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap">
                          {appointment.user.email}
                        </td>

                        <td className="px-6 py-4 whitespace-nowrap text-sm font-medium">
                          {new Date(appointment.date) > new Date() && (
                            <button
                              onClick={() =>
                                cancelAppointment(
                                  appointment.id,
                                  appointment.user.firstName,
                                  appointment.user.lastName,
                                  appointment.user.email,
                                )
                              }
                              className="text-red-500 hover:text-red-700"
                            >
                              <RiDeleteBin6Line className="h-6 w-6" />
                            </button>
                          )}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdminAppointments;
