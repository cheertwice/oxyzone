'use client';
import { useEffect, useState } from 'react';
import { useSession } from 'next-auth/react';
import { RiDeleteBin6Line } from 'react-icons/ri';
import { FaBed, FaChair } from 'react-icons/fa';
import { showToast } from '@/lib/toastUtils';

interface Appointment {
  id: number;
  date: string;
  userId: number;
  chamberType: 'lay' | 'sit';
}

const MyAppointments = () => {
  const { data: session } = useSession();
  const [appointments, setAppointments] = useState<Appointment[]>([]);

  const fetchAppointments = async () => {
    if (session) {
      const response = await fetch(`/api/user/appointments`);
      if (response.ok) {
        const data = await response.json();
        setAppointments(
          data.appointments.sort(
            (a: any, b: any) =>
              new Date(b.date).getTime() - new Date(a.date).getTime(),
          ),
        );
      } else {
        showToast('Chyba při načítání schůzek', 'error');
      }
    }
  };

  useEffect(() => {
    fetchAppointments();
  }, [session, fetchAppointments]); // Add fetchAppointments as a dependency to useEffect

  const cancelAppointment = async (id: number) => {
    if (session) {
      const response = await fetch('/api/user/appointment', {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ id }),
      });

      if (response.ok) {
        await fetchAppointments();
        showToast('Termín úspěšně zrušen', 'success');
      } else {
        const result = await response.json();
        showToast('Chyba při rušení termínu', 'error', result.error);
      }
    }
  };

  return (
    <div className="min-h-screen bg-white flex flex-col pt-6 sm:pt-0 mx-auto">
      <div className="w-full px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg max-w-4xl">
        <h2 className="text-2xl font-semibold text-center text-gray-800 mb-6">
          Moje termíny
        </h2>
        <div className="flex flex-col">
          <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
              <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table className="min-w-full divide-y divide-gray-200">
                  <thead className="bg-gray-50">
                    <tr>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        Datum a čas
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        Typ komory
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        Akce
                      </th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200">
                    {appointments.map((appointment) => (
                      <tr key={appointment.id}>
                        <td className="px-6 py-4 whitespace-nowrap">
                          {new Date(appointment.date).toLocaleString('cs-CZ')}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap">
                          {appointment.chamberType === 'lay' ? (
                            <FaBed className="h-6 w-6 text-blue-500 ml-5" />
                          ) : (
                            <FaChair className="h-6 w-6 text-blue-500 ml-5" />
                          )}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm font-medium">
                          {new Date(appointment.date).getTime() -
                            new Date().getTime() >
                            24 * 60 * 60 * 1000 && (
                            <button
                              onClick={() => cancelAppointment(appointment.id)}
                              className="text-red-500 hover:text-red-700 align-middle"
                            >
                              <RiDeleteBin6Line className="h-6 w-6" />
                            </button>
                          )}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyAppointments;
