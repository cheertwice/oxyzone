'use client';
import { useState, ChangeEvent, FormEvent } from 'react';
import { HiUserCircle } from 'react-icons/hi';
import { MdEmail } from 'react-icons/md';
import { FaUserPlus } from 'react-icons/fa';
import { showToast } from '@/lib/toastUtils';
import ForbiddenPage from '@/components/ForbiddenPage';
import { useSession } from 'next-auth/react';

interface UserData {
  firstName: string;
  lastName: string;
  email: string;
}

const RegisterUser = () => {
  const [userData, setUserData] = useState<UserData>({
    firstName: '',
    lastName: '',
    email: '',
  });
  const [loading, setLoading] = useState(false); // Add loading state
  const { data: session } = useSession();

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setUserData({ ...userData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    setLoading(true); // Set loading to true on form submit

    const response = await fetch('/api/admin/register-user', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(userData),
    });

    setLoading(false); // Set loading to false after request completes

    const result = await response.json();
    if (response.ok) {
      showToast(
        `Uživatel zaregistrován.`,
        'success',
        `E-mail odeslán na ${userData.email}`,
      );
      // Reset form after successful submission
      setUserData({
        firstName: '',
        lastName: '',
        email: '',
      });
    } else {
      showToast('Chyba při registraci uživatele', 'error', result.error);
    }
  };

  if (!session || !session.user.isAdmin) {
    return <ForbiddenPage />;
  }

  return (
    <div className="flex flex-col p-6 md:max-w-md w-full mx-auto md:mx-0 bg-white rounded-lg shadow-md">
      <h2 className="text-xl font-semibold text-gray-800 mb-6">
        Přidat uživatele
      </h2>
      <form onSubmit={handleSubmit} className="space-y-8">
        <div className="flex items-center space-x-3">
          <HiUserCircle className="text-indigo-500 h-5 w-5" />
          <input
            type="text"
            name="firstName"
            required
            placeholder="Jméno"
            className="w-full px-3 py-2 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
            value={userData.firstName}
            onChange={handleChange}
            disabled={loading} // Disable input when loading
          />
        </div>
        <div className="flex items-center space-x-3">
          <HiUserCircle className="text-indigo-500 h-5 w-5" />
          <input
            type="text"
            name="lastName"
            required
            placeholder="Příjmení"
            className="w-full px-3 py-2 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
            value={userData.lastName}
            onChange={handleChange}
            disabled={loading} // Disable input when loading
          />
        </div>
        <div className="flex items-center space-x-3">
          <MdEmail className="text-indigo-500 h-5 w-5" />
          <input
            type="email"
            name="email"
            required
            placeholder="E-mail"
            className="w-full px-3 py-2 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
            value={userData.email}
            onChange={handleChange}
            disabled={loading} // Disable input when loading
          />
        </div>

        <button
          type="submit"
          className={`w-full flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ${
            loading ? 'opacity-50 cursor-not-allowed' : ''
          }`}
          disabled={loading} // Disable button when loading
        >
          <div className="flex items-center space-x-2">
            <FaUserPlus className="text-white h-4 w-4" />
            <span className="inline-flex items-center text-sm font-medium">
              {loading ? 'Odesílání...' : 'Přidat'}
            </span>
          </div>
        </button>
      </form>
    </div>
  );
};

export default RegisterUser;
