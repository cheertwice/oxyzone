'use client';
import Calendar from '@/components/Calendar/Calendar';
import React, { useState, useEffect } from 'react';

const MakeAppointmentPage = () => {
  const [windowWidth, setWindowWidth] = useState(0);
  const [daysAmount, setDaysAmount] = useState(7);

  useEffect(() => {
    setWindowWidth(window.innerWidth);

    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    if (windowWidth >= 1600) {
      setDaysAmount(7);
    } else if (windowWidth >= 1200 && windowWidth < 1600) {
      setDaysAmount(5);
    } else if (windowWidth >= 992 && windowWidth < 1200) {
      setDaysAmount(4);
    } else if (windowWidth >= 768 && windowWidth < 992) {
      setDaysAmount(3);
    } else if (windowWidth > 0) {
      setDaysAmount(2);
    }
  }, [windowWidth]);

  return (
    <div>
      <Calendar
        key={`calendar-${daysAmount}`}
        daysAmount={daysAmount}
        disabled={false}
      />
    </div>
  );
};

export default MakeAppointmentPage;
