import { ChakraProvider, Divider } from '@chakra-ui/react';
import React from 'react';
import Sidebar from '@/components/account/Sidebar';

export default function DashboardLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <ChakraProvider>
      <div className="flex">
        <Sidebar />
        <main className="md:w-4/6 justify-center pt-1 pb-3 pl-2 md:pr-0 pr-2 w-full">
          {children}
        </main>
      </div>
    </ChakraProvider>
  );
}
