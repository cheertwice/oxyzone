'use client';
import { useState, ChangeEvent, FormEvent } from 'react';
import { FaEye } from 'react-icons/fa';
import { IoMdEyeOff } from 'react-icons/io';
import { useSession } from 'next-auth/react';
import { showToast } from '@/lib/toastUtils';

interface FormData {
  currentPassword: string;
  newPassword: string;
  confirmPassword: string;
}

interface ShowPasswords {
  currentPassword: boolean;
  newPassword: boolean;
  confirmPassword: boolean;
}

const ChangePassword = () => {
  const [formData, setFormData] = useState<FormData>({
    currentPassword: '',
    newPassword: '',
    confirmPassword: '',
  });
  const [showPasswords, setShowPasswords] = useState<ShowPasswords>({
    currentPassword: false,
    newPassword: false,
    confirmPassword: false,
  });
  const [loading, setLoading] = useState(false); // Add loading state
  const { data: session } = useSession();

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const togglePasswordVisibility = (field: keyof ShowPasswords) => {
    setShowPasswords({ ...showPasswords, [field]: !showPasswords[field] });
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    if (formData.newPassword === formData.confirmPassword) {
      setLoading(true); // Set loading to true when form is submitted
      const response = await fetch('/api/user/set-new-password', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          oldPassword: formData.currentPassword,
          newPassword: formData.newPassword,
        }),
      });
      setLoading(false); // Set loading back to false when request completes

      const result = await response.json();
      if (response.ok) {
        showToast(result.message, 'success');
        setFormData({
          currentPassword: '',
          newPassword: '',
          confirmPassword: '',
        });
      } else {
        showToast('Chyba při změně hesla', 'error', result.error);
      }
    } else {
      showToast('Hesla se neshodují', 'error');
    }
  };

  return (
    <div className="flex flex-col p-6 md:max-w-md w-full mx-auto md:mx-0 bg-white rounded-lg shadow-md">
      <h2 className="text-xl font-semibold text-gray-800 mb-6">Změna hesla</h2>
      <form onSubmit={handleSubmit} className="space-y-8">
        {Object.entries(formData).map(([key, value]) => (
          <div key={key}>
            <label htmlFor={key} className="text-sm font-medium text-gray-700">
              {key === 'currentPassword'
                ? 'Současné heslo'
                : key === 'newPassword'
                  ? 'Nové heslo'
                  : 'Potvrzení nového hesla'}
            </label>
            <div className="mt-1 relative">
              <input
                type={
                  showPasswords[key as keyof FormData] ? 'text' : 'password'
                }
                id={key}
                name={key}
                required
                className="w-full px-3 py-2 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                value={value}
                onChange={handleChange}
                disabled={loading} // Disable input when loading
              />
              <button
                type="button"
                className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm leading-5"
                onClick={() =>
                  togglePasswordVisibility(key as keyof ShowPasswords)
                }
                disabled={loading} // Disable button when loading
              >
                {showPasswords[key as keyof ShowPasswords] ? (
                  <FaEye className="fill-indigo-600 hover:fill-indigo-500 hover:scale-125" />
                ) : (
                  <IoMdEyeOff className="fill-indigo-600 hover:fill-indigo-500 hover:scale-125" />
                )}
              </button>
            </div>
          </div>
        ))}
        <button
          type="submit"
          className={`w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ${
            loading ? 'opacity-50 cursor-not-allowed' : ''
          }`}
          disabled={loading} // Disable button when loading
        >
          {loading ? 'Odesílání...' : 'Změnit heslo'}
        </button>
      </form>
    </div>
  );
};

export default ChangePassword;
