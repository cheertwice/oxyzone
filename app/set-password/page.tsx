'use client';
import { useState, ChangeEvent, FormEvent, Suspense } from 'react';
import { FaEye } from 'react-icons/fa';
import { IoMdEyeOff } from 'react-icons/io';
import { useRouter } from 'next/navigation';
import { showToast } from '@/lib/toastUtils';
import { useSearchParams } from 'next/navigation';
import { signOut, useSession } from 'next-auth/react';
import { CiLogout } from 'react-icons/ci';

interface FormData {
  password: string;
  confirmPassword: string;
}

interface ShowPasswords {
  password: boolean;
  confirmPassword: boolean;
}

const SetPassword = () => {
  const { data: session } = useSession();
  const searchParams = useSearchParams();
  const router = useRouter();
  const [formData, setFormData] = useState<FormData>({
    password: '',
    confirmPassword: '',
  });
  const [showPasswords, setShowPasswords] = useState<ShowPasswords>({
    password: false,
    confirmPassword: false,
  });

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const togglePasswordVisibility = (field: keyof ShowPasswords) => {
    setShowPasswords({ ...showPasswords, [field]: !showPasswords[field] });
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const token = searchParams.get('token');

    if (token) {
      if (formData.password === formData.confirmPassword) {
        const response = await fetch('/api/user/set-password', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ token: token, password: formData.password }),
        });

        const result = await response.json();
        if (response.ok) {
          showToast('Heslo nastaveno', 'success', 'Můžete se přihlásit');
          router.push('/');
        } else {
          showToast('Chyba při registraci uživatele', 'error', result.error);
        }
      } else {
        showToast('Hesla se neshodují', 'error');
      }
    } else {
      showToast('Chybí autentizační token', 'error');
    }
  };
  const handleLogout = (): void => {
    signOut().then(() => {
      showToast('Úspěšně odhlášeno', 'success');
    });
  };

  return session ? (
    <div
      style={{
        minHeight: '80vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: '25vh',
      }}
    >
      <div className="flex flex-col p-6 md:max-w-md w-full mx-auto md:mx-0 bg-white rounded-lg shadow-md text-center">
        <h2 className="text-xl font-semibold text-gray-800 mb-6">
          Jste přihlášeni
        </h2>
        <button
          type="button"
          className="w-full flex justify-center items-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          onClick={() => {
            handleLogout();
          }}
        >
          <CiLogout className="mr-2" /> Odhlásit se
        </button>
      </div>
    </div>
  ) : (
    <div
      style={{
        minHeight: '80vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: '25vh',
      }}
    >
      <div className="flex flex-col p-6 md:max-w-md w-full mx-auto md:mx-0 bg-white rounded-lg shadow-md">
        <h2 className="text-xl font-semibold text-gray-800 mb-6">
          Nastavit heslo
        </h2>
        <form onSubmit={handleSubmit} className="space-y-8">
          <div>
            <label
              htmlFor="password"
              className="text-sm font-medium text-gray-700"
            >
              Heslo
            </label>
            <div className="mt-1 relative">
              <input
                type={showPasswords.password ? 'text' : 'password'}
                id="password"
                name="password"
                required
                className="w-full px-3 py-2 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                value={formData.password}
                onChange={handleChange}
              />
              <button
                type="button"
                className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm leading-5"
                onClick={() => togglePasswordVisibility('password')}
              >
                {showPasswords.password ? (
                  <FaEye className="fill-indigo-600 hover:fill-indigo-500 hover:scale-125" />
                ) : (
                  <IoMdEyeOff className="fill-indigo-600 hover:fill-indigo-500 hover:scale-125" />
                )}
              </button>
            </div>
          </div>
          <div>
            <label
              htmlFor="confirmPassword"
              className="text-sm font-medium text-gray-700"
            >
              Potvrzení hesla
            </label>
            <div className="mt-1 relative">
              <input
                type={showPasswords.confirmPassword ? 'text' : 'password'}
                id="confirmPassword"
                name="confirmPassword"
                required
                className="w-full px-3 py-2 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                value={formData.confirmPassword}
                onChange={handleChange}
              />
              <button
                type="button"
                className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm leading-5"
                onClick={() => togglePasswordVisibility('confirmPassword')}
              >
                {showPasswords.confirmPassword ? (
                  <FaEye className="fill-indigo-600 hover:fill-indigo-500 hover:scale-125" />
                ) : (
                  <IoMdEyeOff className="fill-indigo-600 hover:fill-indigo-500 hover:scale-125" />
                )}
              </button>
            </div>
          </div>
          <button
            type="submit"
            className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            Nastavit heslo
          </button>
          {/*<button*/}
          {/*  className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"*/}
          {/*  onClick={handleTest}*/}
          {/*>*/}
          {/*  TEST*/}
          {/*</button>*/}
        </form>
      </div>
    </div>
  );
};

export default SetPassword;
