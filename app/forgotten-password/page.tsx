'use client';
import { useState, ChangeEvent, FormEvent } from 'react';
import { useRouter } from 'next/navigation';
import { showToast } from '@/lib/toastUtils';

interface FormData {
  email: string;
}

const ForgottenPasswordPage = () => {
  const router = useRouter();
  const [formData, setFormData] = useState<FormData>({
    email: '',
  });
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [successMessage, setSuccessMessage] = useState<string | null>(null);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
    setErrorMessage(null);
    setSuccessMessage(null);
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();

    try {
      const response = await fetch('/api/user/reset-password', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email: formData.email }),
      });

      if (response.ok) {
        showToast('Odkaz pro reset hesla byl odeslán na váš email.', 'success');
        router.push('/');
      } else {
        const data = await response.json();
        showToast(
          data.error || 'Něco se pokazilo. Zkuste to prosím znovu.',
          'error',
        );
      }
    } catch (error) {
      showToast(
        'Chyba při odesílání požadavku. Zkuste to prosím znovu.',
        'error',
      );
    }
  };

  return (
    <div
      style={{
        minHeight: '80vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: '25vh',
      }}
    >
      <div className="flex flex-col p-6 md:max-w-md w-full mx-auto md:mx-0 bg-white rounded-lg shadow-md">
        <h2 className="text-xl font-semibold text-gray-800 mb-6">
          Zapomenuté heslo
        </h2>
        <form onSubmit={handleSubmit} className="space-y-8">
          <div>
            <label
              htmlFor="email"
              className="text-sm font-medium text-gray-700"
            >
              Email
            </label>
            <div className="mt-1">
              <input
                type="email"
                id="email"
                name="email"
                required
                className="w-full px-3 py-2 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                value={formData.email}
                onChange={handleChange}
              />
            </div>
          </div>
          {errorMessage && (
            <p className="text-sm text-red-600">{errorMessage}</p>
          )}
          {successMessage && (
            <p className="text-sm text-green-600">{successMessage}</p>
          )}
          <button
            type="submit"
            className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            Resetovat heslo
          </button>
        </form>
      </div>
    </div>
  );
};

export default ForgottenPasswordPage;
