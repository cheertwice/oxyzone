import nodemailer from 'nodemailer';
import * as fs from 'fs';
import { join, resolve } from 'path';
import * as util from 'util';

const readFile = util.promisify(fs.readFile);

interface EmailParams {
  senderEmail: string;
  senderPassword: string;
  recipientEmail: string;
  subject: string;
  templateName: string;
  templateData: { [key: string]: string };
}

async function sendEmail({
  senderEmail,
  senderPassword,
  recipientEmail,
  subject,
  templateName,
  templateData,
}: EmailParams) {
  const templateDir = resolve(process.cwd(), 'email-templates');
  const templateFile = join(templateDir, `${templateName}.html`);
  let emailContent = await readFile(templateFile, 'utf8');

  Object.entries(templateData).forEach(([key, value]) => {
    const placeholder = new RegExp(`%${key.toUpperCase()}%`, 'g');
    emailContent = emailContent.replace(placeholder, value);
  });

  const transporter = nodemailer.createTransport({
    host: 'mail.webglobe.cz',
    port: 465,
    secure: true,
    auth: {
      user: senderEmail,
      pass: senderPassword,
    },
    tls: {
      rejectUnauthorized: false,
    },
  });

  try {
    await transporter.sendMail({
      from: `"OXYZONE" <${senderEmail}>`,
      to: recipientEmail,
      subject: subject,
      html: emailContent,
    });

    return { message: 'Email sent successfully' };
  } catch (error) {
    throw new Error('Failed to send email');
  }
}

export default sendEmail;
