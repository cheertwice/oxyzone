import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import './globals.css';
import { ChakraProvider } from '@chakra-ui/react';
import React, { Suspense } from 'react';
import Navbar from '@/components/Navbar';
import Footer from '@/components/Footer';
import NextAuthProvider from '@/app/NextAuthProvider';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'Oxyzone',
  description: 'Webová aplikace firmy Oxyzone',
};

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <NextAuthProvider>
          <ChakraProvider>
            <Navbar />
            <main>
              <Suspense fallback={<div>Načítám...</div>}>{children}</Suspense>
            </main>
            <Footer />
          </ChakraProvider>
        </NextAuthProvider>
      </body>
    </html>
  );
}
