import { NextRequest } from 'next/server';
import sendEmail from '@/app/utils/sendEmail';

export async function POST(request: NextRequest) {
  const { name, contact, message } = await request.json();

  try {
    await sendEmail({
      senderEmail: 'rezervace@oxyzone.cz',
      senderPassword: 'OxyzoneRes1234.',
      recipientEmail: 'Info@oxyzone.cz',
      subject: 'Nová zpráva z kontaktního formuláře',
      templateName: 'newContactFormMessageNotification',
      templateData: {
        name: name,
        contact: contact,
        message: message,
      },
    });

    return new Response(JSON.stringify({ success: true }), {
      status: 201,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  } catch (error) {
    console.error('Error po přijmutí contact formu:', error);
    return new Response(
      JSON.stringify({ success: false, error: 'Server error' }),
      {
        status: 500,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  }
}
