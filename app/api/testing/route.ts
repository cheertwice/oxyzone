import { NextRequest } from 'next/server';
import sendEmail from '@/app/utils/sendEmail';

export async function POST(request: NextRequest) {
  const { email, name } = await request.json();
  const baseUrl = process.env.VERCEL_URL
    ? `https://oxyzone.vercel.app`
    : 'http://localhost:3000';

  const token = '123456789999999999';

  try {
    await sendEmail({
      senderEmail: 'rezervace@oxyzone.cz',
      senderPassword: 'OxyzoneRes1234.',
      recipientEmail: email,
      subject: 'Dokončete registraci',
      templateName: 'registrationEmail',
      templateData: {
        name: name,
        baseUrl: baseUrl,
        token: token,
      },
    });
    return new Response(JSON.stringify({ success: true }), {
      status: 200,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  } catch (error) {
    console.error('Error při nastavování hesla:', error);
    return new Response(JSON.stringify({ error: 'Server error' }), {
      status: 500,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}
