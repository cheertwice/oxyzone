import bcrypt from 'bcrypt';
import { NextRequest } from 'next/server';
import { prisma } from '@/db';

export async function POST(request: NextRequest) {
  const { token, password } = await request.json();

  try {
    const registrationToken = await prisma.registrationToken.findUnique({
      where: {
        token: token,
      },
      include: {
        User: true,
      },
    });

    if (!registrationToken || registrationToken.expires < new Date()) {
      return new Response(
        JSON.stringify({
          error: 'Registrační token je neplatný. Požádejte o nový.',
        }),
        {
          status: 400,
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
    }

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    await prisma.user.update({
      where: {
        id: registrationToken.userId,
      },
      data: {
        password: hashedPassword,
      },
    });

    await prisma.registrationToken.delete({
      where: {
        id: registrationToken.id,
      },
    });

    return new Response(
      JSON.stringify({
        message: 'Heslo úspěšně nastaveno. Můžete se přihlásit.',
      }),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  } catch (error) {
    console.error('Error při nastavování hesla:', error);
    return new Response(JSON.stringify({ error: 'Server error' }), {
      status: 500,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}
