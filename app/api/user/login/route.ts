import { NextRequest } from 'next/server';
import bcrypt from 'bcrypt';
import { prisma } from '@/db';

export async function POST(request: NextRequest) {
  try {
    const { email, password } = await request.json();

    const user = await prisma.user.findUnique({
      where: {
        email: email,
      },
    });

    if (!user || !user.password) {
      return new Response(
        JSON.stringify({ error: 'Zadané údaje jsou nesprávné' }),
        {
          status: 401,
        },
      );
    }

    const isPasswordValid = await bcrypt.compare(password, user.password);

    if (!isPasswordValid) {
      return new Response(JSON.stringify({ error: 'Špatné heslo' }), {
        status: 401,
      });
    }

    return new Response(
      JSON.stringify({
        success: true,
        email: user.email,
        isAdmin: user.isAdmin,
      }),
      {
        status: 200,
      },
    );
  } catch (error) {
    return new Response(JSON.stringify({ error: 'Server error' }), {
      status: 500,
    });
  }
}
