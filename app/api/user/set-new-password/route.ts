import { NextRequest } from 'next/server';
import { getServerSession } from 'next-auth/next';
import { authOptions } from '@/lib/auth';
import { prisma } from '@/db';
import bcrypt from 'bcrypt';

export async function POST(request: NextRequest) {
  const session = await getServerSession(authOptions);
  const { oldPassword, newPassword } = await request.json();

  if (!session) {
    return new Response(JSON.stringify({ error: 'Unauthorized' }), {
      status: 401,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  try {
    const sessionEmail = session?.user?.email;
    if (sessionEmail) {
      const user = await prisma.user.findUnique({
        where: {
          email: sessionEmail,
        },
      });

      if (!user) {
        return new Response(JSON.stringify({ error: 'User not found' }), {
          status: 404,
          headers: {
            'Content-Type': 'application/json',
          },
        });
      }

      if (user.password) {
        const isMatch = await bcrypt.compare(
          oldPassword,
          user.password.toString(),
        );

        if (!isMatch) {
          return new Response(
            JSON.stringify({ error: 'Staré heslo není správně.' }),
            {
              status: 400,
              headers: {
                'Content-Type': 'application/json',
              },
            },
          );
        }

        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(newPassword, salt);

        await prisma.user.update({
          where: {
            id: user.id,
          },
          data: {
            password: hashedPassword,
          },
        });

        return new Response(
          JSON.stringify({ message: 'Heslo úspěšně změněno.' }),
          {
            status: 200,
            headers: {
              'Content-Type': 'application/json',
            },
          },
        );
      }
    }
  } catch (error) {
    console.error('Error updating password:', error);
    return new Response(JSON.stringify({ error: 'Server error' }), {
      status: 500,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}
