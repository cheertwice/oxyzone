import { NextRequest } from 'next/server';
import crypto from 'crypto';
import sendEmail from '../../../utils/sendEmail';
import { prisma } from '@/db';

export async function POST(request: NextRequest) {
  const { email } = await request.json();

  try {
    // Check if the user exists
    const user = await prisma.user.findUnique({
      where: { email: email },
    });

    if (!user) {
      return new Response(
        JSON.stringify({ error: 'Uživatel se zadaným emailem neexistuje' }),
        {
          status: 404,
          headers: { 'Content-Type': 'application/json' },
        },
      );
    }

    // Delete any existing registration token for the user
    await prisma.registrationToken.deleteMany({
      where: { userId: user.id },
    });

    // Delete the current password
    await prisma.user.update({
      where: { email: email },
      data: { password: null },
    });

    // Generate a new token and expiration time
    const token = crypto.randomBytes(32).toString('hex');
    const expires = new Date(Date.now() + 48 * 60 * 60 * 1000); // 48 hours

    // Create a new registration token
    const registrationToken = await prisma.registrationToken.create({
      data: {
        token: token,
        expires: expires,
        userId: user.id,
      },
    });

    // Determine the base URL
    const baseUrl = process.env.VERCEL_URL
      ? `https://oxyzone.vercel.app`
      : 'http://localhost:3000';

    // Send the reset password email
    await sendEmail({
      senderEmail: 'rezervace@oxyzone.cz',
      senderPassword: 'OxyzoneRes1234.',
      recipientEmail: email,
      subject: 'Resetujte si heslo',
      templateName: 'registrationEmail',
      templateData: {
        name: user.firstName,
        baseUrl: baseUrl,
        token: token,
      },
    });

    return new Response(
      JSON.stringify({ success: 'Email k resetu hesla byl odeslán' }),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  } catch (error) {
    console.error('Password reset error:', error);
    return new Response(JSON.stringify({ error: 'Server error' }), {
      status: 500,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}
