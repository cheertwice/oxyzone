import { NextRequest } from 'next/server';
import { prisma } from '@/db';
import { getServerSession } from 'next-auth/next';
import { authOptions } from '@/lib/auth';
import sendEmail from '@/app/utils/sendEmail';

export async function POST(request: NextRequest) {
  const { email, date, chamberType } = await request.json();
  const appointmentUTCDate = new Date(date);
  const now = new Date();
  const isPast = appointmentUTCDate.getTime() < now.getTime();
  const isWithin24Hours =
    appointmentUTCDate.getTime() <= now.getTime() + 24 * 60 * 60 * 1000;
  const isWeekend =
    appointmentUTCDate.getDay() === 0 || appointmentUTCDate.getDay() === 6;

  if (isPast || isWithin24Hours || isWeekend) {
    return new Response(
      JSON.stringify({ success: false, message: 'Zakázaný čas schůzky.' }),
      {
        status: 400,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  }

  try {
    const existingAppointment = await prisma.appointment.findFirst({
      where: {
        date: appointmentUTCDate,
        chamberType: chamberType,
      },
    });

    if (existingAppointment) {
      return new Response(
        JSON.stringify({ success: false, message: 'Slot není volný.' }),
        {
          status: 409,
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
    }

    const user = await prisma.user.findUnique({
      where: {
        email: email,
      },
    });

    if (!user) {
      return new Response(
        JSON.stringify({ success: false, message: 'Uživatel nenalezen' }),
        {
          status: 404,
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
    }

    const newAppointment = await prisma.appointment.create({
      data: {
        date: appointmentUTCDate,
        chamberType: chamberType,
        userId: user.id,
      },
    });

    const formatter = new Intl.DateTimeFormat('cs-CZ', {
      dateStyle: 'short',
      timeStyle: 'short',
    });
    const chamberTypeCZ =
      chamberType === 'sit' ? 'Sedací komora' : 'Lůžková komora';
    await sendEmail({
      senderEmail: 'rezervace@oxyzone.cz',
      senderPassword: 'OxyzoneRes1234.',
      recipientEmail: email,
      subject: 'Termín zarezervován',
      templateName: 'appointmentConfirmation',
      templateData: {
        firstName: user.firstName,
        lastName: user.lastName,
        date: formatter.format(appointmentUTCDate),
        chamberType: chamberTypeCZ,
      },
    });

    await sendEmail({
      senderEmail: 'rezervace@oxyzone.cz',
      senderPassword: 'OxyzoneRes1234.',
      recipientEmail: 'filipromer@gmail.com',
      subject: 'Nový termín zarezervován',
      templateName: 'appointmentCreateNotification',
      templateData: {
        firstName: user.firstName,
        lastName: user.lastName,
        email: email,
        date: formatter.format(appointmentUTCDate),
        chamberType: chamberTypeCZ,
      },
    });

    return new Response(
      JSON.stringify({ success: true, appointment: newAppointment }),
      {
        status: 201, // Created status
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  } catch (error) {
    console.error('Error adding appointment:', error);
    return new Response(
      JSON.stringify({ success: false, error: 'Server error' }),
      {
        status: 500,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  }
}

export async function DELETE(request: NextRequest) {
  const session = await getServerSession(authOptions);
  const { id } = await request.json();

  if (!session?.user?.email) {
    return new Response(
      JSON.stringify({ success: false, error: 'Not authenticated' }),
      {
        status: 401,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  }

  try {
    const appointment = await prisma.appointment.findUnique({
      where: {
        id: id,
      },
      include: {
        user: true,
      },
    });

    if (!appointment || appointment.user.email !== session.user.email) {
      return new Response(
        JSON.stringify({
          success: false,
          message: 'Appointment not found or unauthorized access',
        }),
        {
          status: 404,
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
    }

    const now = new Date();
    const appointmentUTCDate = new Date(appointment.date);
    const isPast = appointmentUTCDate.getTime() < now.getTime();
    const isWithin24Hours =
      appointmentUTCDate.getTime() <= now.getTime() + 24 * 60 * 60 * 1000;

    if (isPast || isWithin24Hours) {
      return new Response(
        JSON.stringify({
          success: false,
          message:
            'Již nelze automaticky zrušit schůzku. Prosím zavolejte nám.',
        }),
        {
          status: 400,
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
    }

    // Delete the appointment
    await prisma.appointment.delete({
      where: {
        id: id,
      },
    });
    const formatter = new Intl.DateTimeFormat('cs-CZ', {
      dateStyle: 'short',
      timeStyle: 'short',
    });
    const chamberTypeCZ =
      appointment.chamberType === 'sit' ? 'Sedací komora' : 'Lůžková komora';
    await sendEmail({
      senderEmail: 'rezervace@oxyzone.cz',
      senderPassword: 'OxyzoneRes1234.',
      recipientEmail: appointment.user.email,
      subject: 'Termín zrušen',
      templateName: 'appointmentCancellation',
      templateData: {
        firstName: appointment.user.firstName,
        lastName: appointment.user.lastName,
        date: formatter.format(appointmentUTCDate),
        chamberType: chamberTypeCZ,
      },
    });

    await sendEmail({
      senderEmail: 'rezervace@oxyzone.cz',
      senderPassword: 'OxyzoneRes1234.',
      recipientEmail: 'filipromer@gmail.com',
      subject: 'Termín zrušen',
      templateName: 'appointmentCancelNotification',
      templateData: {
        firstName: appointment.user.firstName,
        lastName: appointment.user.lastName,
        email: appointment.user.email,
        date: formatter.format(appointmentUTCDate),
        chamberType: chamberTypeCZ,
      },
    });

    return new Response(
      JSON.stringify({
        success: true,
        message: 'Termín úspěšně smazán',
      }),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  } catch (error) {
    console.error('Error při rušení termínu:', error);
    return new Response(
      JSON.stringify({ success: false, error: 'Server error' }),
      {
        status: 500,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  }
}
