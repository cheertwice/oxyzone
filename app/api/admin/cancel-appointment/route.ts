import { NextRequest } from 'next/server';
import { getServerSession } from 'next-auth/next';
import { authOptions } from '@/lib/auth';
import { prisma } from '@/db';
import sendEmail from '@/app/utils/sendEmail';

export async function DELETE(request: NextRequest) {
  const session = await getServerSession(authOptions);
  const { id } = await request.json();

  if (!session?.user?.email) {
    return new Response(
      JSON.stringify({ success: false, error: 'Not authenticated' }),
      {
        status: 401,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  }

  try {
    const appointment = await prisma.appointment.findUnique({
      where: {
        id: id,
      },
      include: {
        user: true,
      },
    });

    if (!appointment) {
      return new Response(
        JSON.stringify({
          success: false,
          message: 'Appointment not found or unauthorized access',
        }),
        {
          status: 404,
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
    }

    // Delete the appointment
    await prisma.appointment.delete({
      where: {
        id: id,
      },
    });

    const formatter = new Intl.DateTimeFormat('cs-CZ', {
      dateStyle: 'short',
      timeStyle: 'short',
    });
    const chamberTypeCZ =
      appointment.chamberType === 'sit' ? 'Sedací komora' : 'Lůžková komora';
    const appointmentUTCDate = new Date(appointment.date);
    await sendEmail({
      senderEmail: 'rezervace@oxyzone.cz',
      senderPassword: 'OxyzoneRes1234.',
      recipientEmail: appointment.user.email,
      subject: 'Termín zrušen',
      templateName: 'appointmentCancellation',
      templateData: {
        firstName: appointment.user.firstName,
        lastName: appointment.user.lastName,
        date: formatter.format(appointmentUTCDate),
        chamberType: chamberTypeCZ,
      },
    });

    await sendEmail({
      senderEmail: 'rezervace@oxyzone.cz',
      senderPassword: 'OxyzoneRes1234.',
      recipientEmail: 'filipromer@gmail.com',
      subject: 'Termín zrušen',
      templateName: 'appointmentCancelNotification',
      templateData: {
        firstName: appointment.user.firstName,
        lastName: appointment.user.lastName,
        email: appointment.user.email,
        date: formatter.format(appointmentUTCDate),
        chamberType: chamberTypeCZ,
      },
    });

    return new Response(
      JSON.stringify({
        success: true,
        message: 'Termín úspěšně smazán',
      }),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  } catch (error) {
    console.error('Error při rušení termínu:', error);
    return new Response(
      JSON.stringify({ success: false, error: 'Server error' }),
      {
        status: 500,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  }
}
