import { prisma } from '@/db';
import { NextRequest } from 'next/server';
import { getServerSession } from 'next-auth/next';
import { authOptions } from '@/lib/auth';
import { Prisma } from '@prisma/client';

export async function GET(request: NextRequest) {
  const session = await getServerSession(authOptions);

  if (!session?.user?.email) {
    return new Response(
      JSON.stringify({ success: false, error: 'Access denied' }),
      {
        status: 403, // Forbidden
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  }

  const searchParams = request.nextUrl.searchParams;
  const amount: string = searchParams.get('amount') || '0';
  const userEmail = searchParams.get('userEmail');
  const amountNumber = parseInt(amount);

  let queryOptions: Prisma.AppointmentFindManyArgs = {
    orderBy: { date: 'desc' },
    include: {
      user: {
        select: {
          firstName: true,
          lastName: true,
          email: true,
        },
      },
    },
  };

  if (amountNumber > 0) {
    queryOptions.take = amountNumber;
  }

  if (userEmail) {
    queryOptions.where = { user: { email: userEmail } };
  }

  try {
    const appointments = await prisma.appointment.findMany(queryOptions);

    return new Response(JSON.stringify({ success: true, appointments }), {
      status: 200,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  } catch (error) {
    console.error('Error fetching appointments:', error);
    return new Response(
      JSON.stringify({ success: false, error: 'Server error' }),
      {
        status: 500,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  }
}
