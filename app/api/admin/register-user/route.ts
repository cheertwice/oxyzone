import { NextRequest } from 'next/server';
import crypto from 'crypto';
import sendEmail from '../../../utils/sendEmail';
import { prisma } from '@/db';

export async function POST(request: NextRequest) {
  const { email, firstName, lastName } = await request.json();

  const token = crypto.randomBytes(32).toString('hex');
  const expires = new Date(Date.now() + 48 * 60 * 60 * 1000);

  try {
    const userWithToken = await prisma.$transaction(async (prisma) => {
      const user = await prisma.user.create({
        data: {
          email: email,
          firstName: firstName,
          lastName: lastName,
        },
      });

      const registrationToken = await prisma.registrationToken.create({
        data: {
          token: token,
          expires: expires,
          userId: user.id,
        },
      });

      return { user, registrationToken };
    });

    const baseUrl = process.env.VERCEL_URL
      ? `https://oxyzone.vercel.app`
      : 'http://localhost:3000';

    await sendEmail({
      senderEmail: 'rezervace@oxyzone.cz',
      senderPassword: 'OxyzoneRes1234.',
      recipientEmail: email,
      subject: 'Dokončete registraci',
      templateName: 'registrationEmail',
      templateData: {
        name: firstName,
        baseUrl: baseUrl,
        token: token,
      },
    });

    await sendEmail({
      senderEmail: 'rezervace@oxyzone.cz',
      senderPassword: 'OxyzoneRes1234.',
      recipientEmail: 'filipromer@gmail.com',
      subject: 'Uživatel zaregistrován',
      templateName: 'userRegistered',
      templateData: {
        firstName: firstName,
        lastName: lastName,
        email: email,
      },
    });

    return new Response(JSON.stringify(userWithToken), {
      status: 200,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  } catch (error) {
    console.error('Registration error:', error);
    return new Response(JSON.stringify({ error: 'Server error' }), {
      status: 500,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}
