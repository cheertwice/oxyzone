import { NextRequest } from 'next/server';
import { prisma } from '@/db';

export async function GET(request: NextRequest) {
  const searchParams = request.nextUrl.searchParams;
  const startDate = searchParams.get('startDate');
  const endDate = searchParams.get('endDate');

  if (!startDate || !endDate) {
    return new Response(
      JSON.stringify({ success: false, error: 'Missing or invalid dates' }),
      {
        status: 400,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  }
  const start = new Date(startDate);
  const end = new Date(endDate);
  end.setHours(23, 59, 0, 0);

  try {
    const appointments = await prisma.appointment.findMany({
      where: {
        AND: [
          {
            date: {
              gte: start,
            },
          },
          {
            date: {
              lte: end,
            },
          },
        ],
      },
    });

    return new Response(JSON.stringify({ success: true, appointments }), {
      status: 200,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  } catch (error) {
    console.error('Error fetching appointments:', error);
    return new Response(
      JSON.stringify({ success: false, error: 'Server error' }),
      {
        status: 500,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  }
}
