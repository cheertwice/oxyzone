import { NextRequest, NextResponse } from 'next/server';
import nodemailer from 'nodemailer';

export async function POST(request: NextRequest) {
  const { recipientEmail, subject, htmlContent } = await request.json();

  const transporter = nodemailer.createTransport({
    host: 'mail.webglobe.cz',
    port: 465,
    secure: true,
    auth: {
      user: 'rezervace@oxyzone.cz',
      pass: 'OxyzoneRes1234.',
    },
    tls: {
      rejectUnauthorized: false,
    },
  });

  try {
    // Send the email
    await transporter.sendMail({
      from: `"OXYZONE" <rezervace@oxyzone.cz>`,
      to: recipientEmail,
      subject: subject,
      html: htmlContent,
    });

    // Return a success response
    return new Response(
      JSON.stringify({ message: 'Email sent successfully' }),
      {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  } catch (error) {
    console.error('Failed to send email:', error);
    return new Response(JSON.stringify({ error: 'Failed to send email' }), {
      status: 500,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}
