'use client';
import { useState, ChangeEvent, FormEvent } from 'react';
import { FaEye } from 'react-icons/fa';
import { IoMdEyeOff } from 'react-icons/io';
import { signIn } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import { showToast } from '@/lib/toastUtils';

interface FormData {
  email: string;
  password: string;
}

const LoginPage = () => {
  const router = useRouter();
  const [formData, setFormData] = useState<FormData>({
    email: '',
    password: '',
  });
  const [showPassword, setShowPassword] = useState(false);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const handleForgottenPassword = (e: FormEvent) => {
    router.push('/forgotten-password');
  };

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    signIn('credentials', {
      redirect: false,
      email: formData.email,
      password: formData.password,
    })
      .then((result) => {
        if (result?.error) {
          showToast('Špatný email nebo heslo', 'error');
        } else {
          router.push('/');
          showToast('Úspěšně přihlášeno', 'success');
        }
      })
      .catch(() => {
        showToast('Špatný email nebo heslo', 'error');
      });
  };

  return (
    <div
      style={{
        minHeight: '80vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: '25vh',
      }}
    >
      <div className="flex flex-col p-6 md:max-w-md w-full mx-auto md:mx-0 bg-white rounded-lg shadow-md">
        <h2 className="text-xl font-semibold text-gray-800 mb-6">Login</h2>
        <form onSubmit={handleSubmit} className="space-y-8">
          <div>
            <label
              htmlFor="email"
              className="text-sm font-medium text-gray-700"
            >
              Email
            </label>
            <div className="mt-1">
              <input
                type="email"
                id="email"
                name="email"
                required
                className="w-full px-3 py-2 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                value={formData.email}
                onChange={handleChange}
              />
            </div>
          </div>
          <div>
            <label
              htmlFor="password"
              className="text-sm font-medium text-gray-700"
            >
              Heslo
            </label>
            <div className="mt-1 relative">
              <input
                type={showPassword ? 'text' : 'password'}
                id="password"
                name="password"
                required
                className="w-full px-3 py-2 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                value={formData.password}
                onChange={handleChange}
              />
              <button
                type="button"
                className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm leading-5"
                onClick={togglePasswordVisibility}
              >
                {showPassword ? (
                  <FaEye className="fill-indigo-600 hover:fill-indigo-500 hover:scale-125" />
                ) : (
                  <IoMdEyeOff className="fill-indigo-600 hover:fill-indigo-500 hover:scale-125" />
                )}
              </button>
            </div>
          </div>
          <div className="flex justify-end">
            <button
              type="button"
              onClick={handleForgottenPassword}
              className="text-sm text-indigo-600 hover:text-indigo-500 focus:outline-none"
            >
              Zapomenuté heslo?
            </button>
          </div>
          <button
            type="submit"
            className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            Přihlásit
          </button>
        </form>
      </div>
    </div>
  );
};

export default LoginPage;
