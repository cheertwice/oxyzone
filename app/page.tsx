'use client';
import React, { useEffect, useState } from 'react';
import HeroSection from '@/components/HomePage/HeroSection';
import PricingSection from '@/components/HomePage/PricingSection';
import ContactSection from '@/components/HomePage/ContactSection';
import Calendar from '@/components/Calendar/Calendar';
import ContactFormSection from '@/components/HomePage/ContactFormSection';

function Page() {
  const [windowWidth, setWindowWidth] = useState(0);
  const [daysAmount, setDaysAmount] = useState(7);
  useEffect(() => {
    setWindowWidth(window.innerWidth);

    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    if (windowWidth >= 1600) {
      setDaysAmount(7);
    } else if (windowWidth >= 1200 && windowWidth < 1600) {
      setDaysAmount(5);
    } else if (windowWidth >= 992 && windowWidth < 1200) {
      setDaysAmount(4);
    } else if (windowWidth >= 768 && windowWidth < 992) {
      setDaysAmount(3);
    } else if (windowWidth > 0) {
      setDaysAmount(2);
    }
  }, [windowWidth]);
  return (
    <div className="App">
      <HeroSection />
      <PricingSection />
      <h2 className="text-center mb-10 text-3xl font-bold text-gray-900 mt-10">
        Kalendář termínů
      </h2>
      {/*<Calendar*/}
      {/*  key={`calendar-${daysAmount}`}*/}
      {/*  daysAmount={daysAmount}*/}
      {/*  disabled={true}*/}
      {/*/>*/}
      <ContactFormSection />
      <ContactSection />
    </div>
  );
}

export default Page;
