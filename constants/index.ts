import { FaCalendarPlus } from 'react-icons/fa';
import { FiCalendar, FiKey } from 'react-icons/fi';
import { RiAdminFill } from 'react-icons/ri';

export const NAV_LINKS = [
  { href: '/faq', key: 'faq', label: 'FAQ' },
  { href: '/#pricing', key: 'pricing', label: 'Ceník' },
  // { href: '/login', key: 'login', label: 'Přihlášení' },
];

export const ACCOUNT_LINKS = [
  {
    name: 'Rezervace termínu',
    href: '/account/make-appointment',
    icon: FaCalendarPlus,
  },
  { name: 'Moje termíny', href: '/account/appointments', icon: FiCalendar },
  { name: 'Změnit heslo', href: '/account/change-password', icon: FiKey },
];

export const ADMIN_LINKS = [
  {
    name: 'Přidat uživatele',
    href: '/account/register-user',
    icon: RiAdminFill,
  },
  {
    name: 'Správa termínů',
    href: '/account/manage-appointments',
    icon: RiAdminFill,
  },
];
