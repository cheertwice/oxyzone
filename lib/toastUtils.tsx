import { createStandaloneToast } from '@chakra-ui/react';

const { toast } = createStandaloneToast();

type ToastStatus = 'info' | 'warning' | 'success' | 'error';

export const showToast = (
  message: string,
  status: ToastStatus,
  description?: string,
): void => {
  toast({
    title: message,
    status: status,
    position: 'top',
    duration: 5000,
    isClosable: true,
    description: description,
  });
};
