import { NextAuthOptions } from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';

export const authOptions: NextAuthOptions = {
  providers: [
    CredentialsProvider({
      name: 'Credentials',
      credentials: {
        email: {},
        password: {},
      },
      async authorize(credentials, req) {
        const body = JSON.stringify({
          email: credentials?.email,
          password: credentials?.password,
        });

        const baseUrl = process.env.VERCEL_URL
          ? `https://oxyzone.vercel.app`
          : 'http://localhost:3000';

        const res = await fetch(`${baseUrl}/api/user/login`, {
          method: 'POST',
          body: body,
          headers: {
            'Content-Type': 'application/json',
          },
        });

        const user = await res.json();

        if (res.ok && user) {
          return { id: user.id, email: user.email, isAdmin: user.isAdmin };
        }
        return null;
      },
    }),
  ],
  callbacks: {
    async jwt({ token, user }) {
      if (user) token.isAdmin = user.isAdmin;
      return token;
    },
    async session({ session, token }) {
      if (session?.user) session.user.isAdmin = token.isAdmin;
      return session;
    },
  },
};
